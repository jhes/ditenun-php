<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ProductController extends Controller
{
    protected $client;

    /**
     * ProductController constructor
     *
     * @return void
     */
    public function __construct()
    {
        // Fetching products from Ditenun API
        $this->client = new Client([
            // Base URI is used with relative requests
            // This is my base URI for API, maybe different in your side
            'base_uri' => 'http://localhost:8080',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $response = $this->client->get('products');

        // NOTE: HTTP response can be 200, which mean OK and we get the list of products
        // Or maybe 404, which mean not found. So we need to check the response first
        if($response->getStatusCode() != 200)
        {
            abort(404);
        }

        $products = json_decode($response->getBody()->getContents());

        return view ('products.index', [
            'products' => $products
        ]);
    }
}
