@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Your Cart</div>
                    <div id="cart-display" class="card-body">
                        loading...
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Products</div>
                    <div class="card-body">
                        @foreach($products as $product)
                            {{ $product->name }}: IDR {{ $product->price }} <button class="add-to-cart pull-right" data-item="{{ $product->name }}" data-price="{{ $product->price }}">Add to Cart</button>
                            <hr>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>
        $(function() {
            // Add to cart
            $(".add-to-cart").click(function(){
                axios.post('http://localhost:8080/cart', {
                    item: "" + $(this).data("item"),
                    price: "" + $(this).data("price"),
                })
                .then(function (response) {
                    retrieveMyCart()
                })
                .catch(function (error) {
                    console.log(error);
                });
            });

            // Load my cart
            retrieveMyCart()

            function retrieveMyCart() {
                axios.get('http://localhost:8080/cart')
                .then(function (response) {
                    if(response.data.item === "") {
                        $("#cart-display").text("Your cart is empty");
                    } else {
                        $("#cart-display").text(response.data.item + ": " + response.data.price);
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        });
       
    </script>
@endsection